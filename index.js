const express = require("express");
const { PrismaClient } = require('@prisma/client')
const {todoSchema, updateTodoSchema} = require("./validator/todoSchema")


const prisma = new PrismaClient();
const app = express();
app.use(express.json());

//returns list of todos
app.get('/todos', (req, res) => {
    prisma.todos.findMany().then(data => {
        res.status(200).json(data);
    })
});

//returns the todo matching the id
app.get('/todos*', (req, res) => {

    const searchId = parseInt(req.url.slice(7)); //extracting integer value from url
    prisma.todos.findUnique({
        where:{
            id:searchId
        },
    }).then( data=>{
        if(data === null){
            throw new Error("Todo not found with id:", searchId);
        }
        res.status(201).json(data)

     }).catch( err=> res.status(404).json({"message":"Not found"}) )
})

//delete the todo matching the id
app.delete('/todos*', (req, res) => {
    const searchId = parseInt(req.url.slice(7)); //extracting the integer value ID from the url
    prisma.todos.delete({
        where: {
            id: searchId
        },
    }).then(data=>{
        if(data === null){
            throw new Error("Todo not found with id:", searchId);
        }
        res.status(201).json(data)
    }).catch( err=> res.status(404).json({"message":"Not found"}) )
});

//creates a new todo
app.post('/todos', (req, res) => {
    todoSchema.validate(req.body).then((data)=>{
        
        prisma.todos.create({
            data: data
        }).then((data) => res.status(200).json(data))

    }).catch(err=>res.status(400).json({"message":err["message"]}));
});

//updates the todo matching the id
app.put('/todos', (req, res)=>{
    updateTodoSchema.validate(req.body).then((todo)=>{
        prisma.todos.update({
            where:{
                id: parseInt(todo.id)
            },
            data: {
                text: todo.text,
                isCompleted: todo.isCompleted
            },
        }).then((data)=> {

            if(data === null){
                throw new Error("Todo not found with id:", id);
            }
            res.status(201).json(data)
        }).catch(err=>res.status(408).json({"message": "Not found"}))
    }).catch(err=>res.status(400).json({"message":err["message"]}))

    
});

app.listen(3000)